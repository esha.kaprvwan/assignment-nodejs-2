"use strict";
function combine(n1, n2) {
    let result;
    if (typeof n1 === 'number' && typeof n2 === 'number') {
        const result = n1 + n2;
    }
    else {
        result = n1.toString() + n2.toString();
    }
    return result;
}
const combinedAges = combine(30, 26);
console.log(combinedAges);
const combinesNames = combine('MAx', 'Anna');
console.log(combinesNames);
