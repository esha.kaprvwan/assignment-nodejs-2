"use strict";
console.log('your code goes here');
function add1(n1, n2, showResult, phrase) {
    // if(typeof n1 !=='number' || typeof n2!=='number'){
    //     throw new Error('Incorrect input!')
    // }
    const result = n1 + n2;
    if (showResult) {
        console.log(phrase + result);
    }
    else {
        return n1 + n2;
    }
}
const num1 = 5;
const num2 = 2.8;
const printResult = true;
const resultPhrase = 'Result is:';
const result = add1(num1, num2, printResult, resultPhrase);
console.log(result);
