import express from "express";
import jsonwebtoken from "jsonwebtoken";
import { config } from "dotenv";

config()
const app=express();
const posts=[
{
    username:'kyle',
    title:'Post 1'
},
{
    username:'esha',
    title:'Post 2'
}
]
app.use(express.json())
// app.get('/posts',authenticate,(req,res)=>{
// res.json(posts.filter(post=>post.username==req.user.name))
// })
let refreshTokens=[]
app.post('/token',(req,res)=>{
    const refreshToken=req.body.token
    if(refreshToken==null) return res.sendStatus(401)
    if(!refreshTokens.includes(refreshToken)) return res.sendStatus(401)
    jsonwebtoken.verify(refreshToken.process.env.REFRESH_TOKEN_SECRET)
    if(err) return res.sendStatus(403)
    const accessToken=generateAccessToken({ name:user.name})
    res.json({  accessToken: accessToken})

})
app.post('/login',(req,res)=>{
    //autheticate user
    const username=req.body.username
    const user={name: username}
    const accessToken=generateAccessToken(user)
    const refreshToken =jsonwebtoken.sign(user,process.env.REFRESH_TOKEN_SECRET)
    refreshTokens.push(refreshToken)
    res.json({ accessToken: accessToken, refreshToken:refreshToken})
})
// function authenticate(req,res,next){
//     const authHeader=req.headers['authorization']
//     const token=authHeader && authHeader.split(' ')[1]
//     if(token==null) return res.sendStatus(400)
//     jsonwebtoken.verify(token,process.env.ACCESS_TOKEN_SECRET,(err,user)=>{
//         if(err) return res.sendStatus(403)
//         res.user=user
//         next()
//     })
// }
app.delete('/logout',(req,res)=>{
    refreshTokens=refreshTokens.filter(token=> token!==req.body.token)
    res.sendStatus(204)
})
function generateAccessToken(user){
    return jsonwebtoken.sign(user,process.env.ACCESS_TOKEN_SECRET,{expiresIn:'15s'})
   
}
app.listen(6090)