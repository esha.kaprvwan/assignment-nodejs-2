import express from "express";
import jsonwebtoken from "jsonwebtoken";
import { config } from "dotenv";
config()
const app=express();
const posts=[
{
    username:'kyle',
    title:'Post 1'
},
{
    username:'esha',
    title:'Post 2'
}
]
app.use(express.json())
app.get('/posts',authenticate,(req,res)=>{
res.json(posts.filter(post=>post.username==req.user.name))
})
// app.post('/login',(req,res)=>{
//     //autheticate user
//     const username=req.body.username
//     const user={name: username}
//     const accessToken=jsonwebtoken.sign(user, process.env.ACCESS_TOKEN_SECRET)
//     res.json({ accessToken: accessToken})
// })
function authenticate(req,res,next){
    const authHeader=req.headers['authorization']
    const token=authHeader && authHeader.split(' ')[1]
    if(token==null) return res.sendStatus(400)
    jsonwebtoken.verify(token,process.env.ACCESS_TOKEN_SECRET,(err,user)=>{
        if(err) return res.sendStatus(403)
        res.user=user
        next()
    })
}
app.listen(3000)