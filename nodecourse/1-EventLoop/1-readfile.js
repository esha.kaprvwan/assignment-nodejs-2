const { readFile, writeFile } = require('fs')

console.log('started')

readFile('./content/first.txt', 'utf8', (err, result) => {
  if (err) {
    console.log(err)
    return
  }
  console.log(result)
  console.log('completed the task')
})
console.log('starting next task')
