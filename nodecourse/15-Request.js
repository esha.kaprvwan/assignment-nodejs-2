const http = require('http')

// const server = http.createServer((req, res) => {
//   res.end('Welcome')
// })


const app = http.createServer()
// emits request event

app.on('request', (req, res) => {
  res.end('Welcome')
})

app.listen(5000)
