const { createReadStream } = require('fs')

const Rstream = createReadStream('./content/test2.txt')

Rstream.on('data', (result) => {
  console.log(result)
})
Rstream.on('error', (err) => console.log(err))
